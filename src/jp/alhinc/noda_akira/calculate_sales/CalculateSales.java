package jp.alhinc.noda_akira.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class CalculateSales {
	public static void main(String[] args) throws IOException {

		if(args.length != 1){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		HashMap<String, String> branchNameMap = new HashMap<String, String>();
		HashMap<String, Long> valueMap = new HashMap<String, Long>();

		//////支店定義ファイル////////////////////////////////////////////////////
		if(!readBranchList(args[0], "branch.lst", branchNameMap, valueMap)){
			return;
		}
		///////////////売り上げファイル////////////////////////////////////////////////////
		if(!valueFileInput(args[0], branchNameMap, valueMap)) {
			return;
		}
		/////////////売上集計ファイル出力//////////////////////////////////////////////////
		if(!fileOutput(args[0], "branch.out",branchNameMap ,valueMap)) {
			return;
		}
	}

	public static boolean readBranchList(String path, String branchListFile, HashMap<String, String> nameMap, HashMap<String, Long> salesMap){
		BufferedReader br = null;
		try {
			File file = new File(path, branchListFile);
			if(!file.exists()){
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			while((line = br.readLine()) != null) {
				String[] number = line.split(",");
				if(!number[0].matches("^[0-9]{3}$")){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				if(number.length != 2 ){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				nameMap.put(number[0], number[1]);
				salesMap.put(number[0], 0L);
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					return false;
				}
			}
		}
		return true;
	}

	public static boolean valueFileInput(String path, HashMap<String, String> nameMap, HashMap<String, Long> salesMap){
		// 一致したファイル名を格納するために生成
		ArrayList<String> matchFileList = new ArrayList<String>();
		int i;
		int j;
		File dir = new File(path);
		File[] file2 = dir.listFiles();
		for(i = 0; i < file2.length; i++) {
			String matchFileName = file2[i].getName();
			if(matchFileName.matches("^[0-9]{8}\\.(rcd)$") && file2[i].isFile()) {
				matchFileList.add(matchFileName);
			}
		}
		for(j = 0; j < matchFileList.size() -1; j++){
			String[] number2 = matchFileList.get(j).split("\\.");
			String[] number3 = matchFileList.get(j + 1).split("\\.");
			long longCode = Long.parseLong(number2[0]);
			long longCode2 = Long.parseLong(number3[0]);
			if(longCode2 - longCode != 1){
				System.out.println("売上ファイル名が連番になっていません");
				return false;
			}
		}
		 // マッチした回数
		for(j = 0; j < matchFileList.size(); j++) {
			// 一致したファイルのデータを格納するために生成
			ArrayList<String> rcdList = new ArrayList<String>();
			BufferedReader br1 = null;
			try {
				File want = new File(path,matchFileList.get(j));
				FileReader fr1 = new FileReader(want);
				br1 = new BufferedReader(fr1);
				String line2;
				while((line2 = br1.readLine()) != null) {
					rcdList.add(line2);
				}
				if(rcdList.size() != 2) {
					System.out.println(matchFileList.get(j) + "のフォーマットが不正です");
					return false;
				}
				if(!rcdList.get(1).matches("^[0-9]*$")) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
				if(!nameMap.containsKey(rcdList.get(0))) {
					System.out.println(matchFileList.get(j) + "の支店コードが不正です");
					return false;
				}
				long a = salesMap.get(rcdList.get(0));
				long b = Long.parseLong(rcdList.get(1));
				long c = a + b ;
				if(String.valueOf(c).length() > 10) {
					System.out.println("合計金額が10桁を超えました");
					return false;
				}
				salesMap.put(rcdList.get(0),c);
			}catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}finally {
				if(br1 != null) {
					try {
						br1.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return false;
					}
				}
			}

		}
		return true;
	}

	public static boolean fileOutput(String path, String outPutFile, HashMap<String, String> nameMap, HashMap<String, Long> salesMap) {
		BufferedWriter bw = null;
		try {
			File ff = new File(path,outPutFile);
			FileWriter fw = new FileWriter(ff);
			bw = new BufferedWriter(fw);
			for (Entry<String, String> entry : nameMap.entrySet()) {
				bw.write(entry.getKey() + "," + entry.getValue() + "," + salesMap.get(entry.getKey()));
				bw.newLine();
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");;
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println("7予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}
